// WellKnownGeometry Test Suite by Roma Hicks is marked with CC0 1.0 Universal.
// To view a copy of this license, visit http://creativecommons.org/publicdomain/zero/1.0

package wellknowngeometry

import (
	"bytes"
	"testing"
)

func TestSwapByteOrder(t *testing.T) {
	t.Run("Convert byte array from big endian to little endian.", func(t *testing.T) {
		wkb := []byte{0, 255, 100, 5, 1, 0, 0, 3}
		expectedValue := []byte{3, 0, 0, 1, 5, 100, 255, 0}
		recievedValue := SwapByteOrder(wkb)
		if bytes.Equal(expectedValue, recievedValue) != true {
			t.Errorf("Converted data did not meet expected byte order.\n%v\n", expectedValue)
		}
	})
	t.Run("An empty array should return an empty array.", func(t *testing.T) {
		var wkb []byte
		var expectedValue []byte
		recievedValue := SwapByteOrder(wkb)
		if bytes.Equal(expectedValue, recievedValue) != true {
			t.Errorf("Converted data did not meet expected byte order.\n%v\n", expectedValue)
		}
	})
}

func TestDoubleFromBinary(t *testing.T) {
	t.Run("Make a double float number from a set of bytes. Little endian", func(t *testing.T) {
		wkb := []byte{0x29, 0xad, 0x17, 0xe0, 0xe2, 0x25, 0x12, 0x41}
		expectedValue := 297336.7188403183
		recievedValue := doubleFromBinary(wkb, 1)
		if expectedValue != recievedValue {
			t.Errorf("Recieved value, %v, did not match expected value: %v.\n", recievedValue, expectedValue)
		}
	})
	t.Run("Make a double float number from a set of bytes Big endian.", func(t *testing.T) {
		wkb := []byte{0x41, 0x12, 0x25, 0xe2, 0xe0, 0x17, 0xad, 0x29}
		expectedValue := 297336.7188403183
		recievedValue := doubleFromBinary(wkb, 0)
		if expectedValue != recievedValue {
			t.Errorf("Recieved value, %v, did not match expected value: %v.\n", recievedValue, expectedValue)
		}
	})
}
