// Copyright 2020 Roma Hicks

// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE..

package wellknowngeometry

import (
	"fmt"
	//"log"
	"strings"
)

// wkbPoint_ZM creates a WKT 3D/2DM Point with an array of WKB bytes.
func wkbPoint_ZM(wkb []byte) (string, int) {
	geoString, bCount := point_zm(wkb[5:], wkb[0])
	return geoString, bCount + 5
}

// point_zm creates a 3D/2DM Point from an array of bytes.
func point_zm(wkb []byte, endian uint8) (string, int) {
	wkt := strings.Builder{}
	wkt.WriteString(fmt.Sprintf("%.8f", doubleFromBinary(wkb[0:8], endian)))
	wkt.WriteString(" ")
	wkt.WriteString(fmt.Sprintf("%.8f", doubleFromBinary(wkb[8:16], endian)))
	wkt.WriteString(" ")
	wkt.WriteString(fmt.Sprintf("%.8f", doubleFromBinary(wkb[16:24], endian)))
	wkt.WriteString(" ")
	wkt.WriteString(fmt.Sprintf("%.8f", doubleFromBinary(wkb[24:32], endian)))
	return wkt.String(), 32
}

// wkbLineString_ZM creates a WKT 2D Linestring with an array of WKB bytes.
func wkbLineString_ZM(wkb []byte) (string, int) {
	geoString, bCount := lineString_zm(wkb[5:], wkb[0])
	return geoString, bCount + 5
}

// lineString_zm creates a 2D LineString from an array of bytes.
func lineString_zm(wkb []byte, endian uint8) (string, int) {
	wkt := strings.Builder{}
	var byteCount int
	var i uint32

	childCount := get32Number(wkb[:4], endian)
	byteCount += 4

	for i = 0; i < childCount; i++ {
		geoString, bCount := point_zm(wkb[byteCount:], endian)
		byteCount += bCount
		wkt.WriteString(geoString)
		if i < childCount-1 {
			wkt.WriteString(", ")
		}
	}

	return wkt.String(), byteCount
}

// wkbPolygon_ZM creates a WKT 2D Polygon with an array of WKB bytes.
func wkbPolygon_ZM(wkb []byte) (string, int) {
	geoString, bCount := polygon_zm(wkb[5:], wkb[0])
	return geoString, bCount + 5
}

// polygon_z_m constructs a 2D Polygon from an array of bytes.
func polygon_zm(wkb []byte, endian uint8) (string, int) {
	wkt := strings.Builder{}
	var byteCount int
	var i uint32

	childCount := get32Number(wkb[:4], endian)
	byteCount += 4

	for i = 0; i < childCount; i++ {
		wkt.WriteString("(")
		geoString, bCount := lineString_zm(wkb[byteCount:], endian)
		byteCount += bCount
		wkt.WriteString(geoString)
		wkt.WriteString(")")
		if i < childCount-1 {
			wkt.WriteString(", ")
		}
	}

	return wkt.String(), byteCount
}

// Constructs a GeometryCollection_ZM from an array of bytes.
func wkbGeometryCollection_ZM(wkb []byte) (string, int) {
	wkt := strings.Builder{}
	var byteCount int
	var i uint32

	childCount := get32Number(wkb[5:9], wkb[0])
	byteCount += 9

	for i = 0; i < childCount; i++ {
		//log.Print(byteCount)
		geoString, bCount := WKBToWKT(wkb[byteCount:])
		byteCount += bCount
		//log.Print(byteCount)
		wkt.WriteString(geoString)
		if i < childCount-1 {
			wkt.WriteString(", ")
		}
	}

	return wkt.String(), byteCount
}

// Constructs a PolyhedralSurface from an array of bytes.
func wkbPolyhedralSurfaceTIN_ZM(wkb []byte) (string, int) {
	wkt := strings.Builder{}
	var byteCount int
	var i uint32

	childCount := get32Number(wkb[5:9], wkb[0])
	byteCount += 9

	for i = 0; i < childCount; i++ {
		wkt.WriteString("(")
		geoString, bCount := wkbPolygon_ZM(wkb[byteCount:])
		byteCount += bCount
		wkt.WriteString(geoString)
		wkt.WriteString(")")
		if i < childCount-1 {
			wkt.WriteString(", ")
		}
	}

	return wkt.String(), byteCount
}
